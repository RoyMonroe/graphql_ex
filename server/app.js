
// a05bc9c5-d85f-4991-a71b-e65523075c6d
const express = require("express");
const { graphql } = require("graphql");
const { graphqlHTTP } = require("express-graphql");
const schema = require("./schema/schema");
const mongoose = require("mongoose");
const cors = require ('cors')

const uri =
  "mongodb+srv://RoyMonroe:Pdpgdp60@firstapp.lnlfp.mongodb.net/firstdb?retryWrites=true&w=majority";

const app = express();
const PORT = 3005;

async function start() {
  try {
    await mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });
    app.listen(PORT, (err) => {
      err ? console.log(error) : console.log("Server started!");
    });
    app.use(cors())
    app.use(
      "/graphql",
      graphqlHTTP({
        schema,
        graphiql: true,
      })
    );
  } catch (e) {
    console.log("Server Error", e.message);
    process.exit(1);
  }
}

start()